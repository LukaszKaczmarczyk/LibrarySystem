package LibrarySimulator.Products;

public class Magazine extends Book {

    String number;

    public Magazine(String title, String quantity, String number) {
        super(title,quantity);
        this.number = number;
    }
}
