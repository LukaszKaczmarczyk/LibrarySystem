package LibrarySimulator.Products;

public class BorrowItem {

    private String cardId;
    private String title;
    private String authorNumber;

    public BorrowItem(String cardId, String title, String authorNumber) {
        this.cardId = cardId;
        this.title = title;
        this.authorNumber = authorNumber;
    }

    public String getCardId() {
        return cardId;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthorNumber() {
        return authorNumber;
    }

    public BorrowItem(String cardId) {
        this.cardId = cardId;
    }
}
