package LibrarySimulator.Products;

import LibrarySimulator.Utils.RegexExpressions;
import LibrarySimulator.Utils.Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {

    private String absolutePath = new File("").getAbsolutePath();
    private String path = absolutePath + "\\src\\LibrarySimulator\\Resources\\";
    private static final int MAX_NUMBER_FOR_BORROW_STUDENTS = 4;
    private static final int MAX_NUMBER_FOR_BORROW_PROFESORS = 10;

    List<Book> books = new ArrayList<>();
    List<Magazine> magazines = new ArrayList<>();
    List<User> users = new ArrayList<>();
    List<BorrowItem> borrowItems = new ArrayList<>();

    public void runInterfaceMenu() throws IOException {
        Library library = new Library();
        Scanner in = new Scanner(System.in);
        String option = "";

        boolean quit = false;
        library.readFile("books");
        library.readFile("magazines");
        library.readFile("users");
        library.readFile("borrowItems");

        Utils.displayOptionMenu();

        while (!quit) {
            System.out.println();
            option = in.nextLine();
            switch (option) {
                case "1":
                    System.out.println("Option 'Display all books' have been choosen");
                    library.displayBooks();
                    break;
                case "2":
                    System.out.println("Option 'Display all magazines' have been choosen");
                    library.displayMagazines();
                    break;
                case "3":
                    System.out.println("Option 'Add book' have been choosen");
                    System.out.println("Put the title");
                    String bookTitle = in.nextLine();
                    System.out.println("Put the author");
                    String bookAuthor = in.nextLine();
                    System.out.println("Put the number");
                    String quantity = in.nextLine();
                    library.addBook(bookTitle, bookAuthor, quantity);
                    break;
                case "4":
                    System.out.println("Option 'Add magazine to library' have been choosen");
                    System.out.println("Put the title");
                    String magazineTitle = in.nextLine();
                    System.out.println("Put the magazine number (format mm/yyyy)");
                    String magazineNumber = in.nextLine();
                    System.out.println("Put the quantity");
                    String quantityMagazines = in.nextLine();
                    library.addMagazine(magazineTitle, magazineNumber, quantityMagazines);
                    break;
                case "5":
                    System.out.println("Option 'Display users' have been choosen");
                    library.displayUsers();
                    break;
                case "6":
                    System.out.println("Option 'Add user' have been choosen");
                    System.out.println("Name");
                    String name = in.nextLine();
                    System.out.println("Surname");
                    String surname = in.nextLine();
                    System.out.println("Please put the number of library card");
                    String idCard = in.nextLine();
                    System.out.println("Please put 'P' for Profesor or 'S' for Student?");
                    String typ = in.nextLine();
                    if ((typ.equals("P")) || (typ.equals("S"))) {
                        library.addUsers(name, surname, idCard, typ);
                    } else {
                        System.out.println("Invalid value. If you would like to try again please put 6");
                    }
                    break;
                case "7":
                    System.out.println("Option 'Open file with borrowed items' have been choosen");
                    library.displayBorrowedOnes();
                    break;
                case "8":
                    System.out.println("Option 'Borrow the book/magazine' have been choosen");
                    System.out.println("Please put id of library card");
                    String userIdCard = in.nextLine();
                    if (!library.isUserIsAbleToBorrow(userIdCard)) {
                        System.out.println("Exceeded limit");
                    } else {
                        System.out.println("Title");
                        String title = in.nextLine();
                        System.out.println("Find author of books or number of magazine");
                        String authorNumber = in.nextLine();
                        library.borrowItem(userIdCard, title, authorNumber);
                    }
                    break;
                case "9":
                    quit = true;
                    break;
                default:
                    System.out.println("Invalid option. Try again.");
            }
        }
        System.out.println();
        System.out.println("Library simulator has been closed");
    }

    public void addBook(String bookTitle, String bookAuthor, String booksQuantity) throws IOException {

        boolean isPresent = false;
        for (Book allBook : books) {
            if ((allBook.getTitle().equals(bookTitle)) && (allBook.getAuthor().equals(bookAuthor))) {
                allBook.quantity = Integer.toString(Integer.parseInt(allBook.quantity) + Integer.parseInt(booksQuantity));
                isPresent = true;
            }
        }
        if (!isPresent) {
            if (bookAuthor.matches(RegexExpressions.getRegexName()) && booksQuantity.matches(RegexExpressions.getRegexQuantity())) {
                books.add(new Book(bookTitle, bookAuthor, booksQuantity));
            } else
                System.out.println("Incorrect value has been put. For author name you can only use letters, for quantity only numbers from range '1-1000')");
        }
        saveBooksIntoFile();
    }

    public void saveBooksIntoFile() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(path + "books.txt"));
        String contentFile = "Title;Author;Quantity\r\n";
        try {

            for (Book allBook : books) {
                contentFile += (allBook.getTitle() + ";" + allBook.getAuthor() + ";" + allBook.quantity + "\r\n");
            }

            bw.write(contentFile);
        } finally {
            bw.close();
        }
    }

    public void addMagazine(String title, String number, String quantity) throws IOException {
        boolean isPresent = false;
        for (Magazine allMagazine : magazines) {
            if ((allMagazine.getTitle().equals(title)) && (allMagazine.number.equals(number))) {
                allMagazine.quantity = Integer.toString(Integer.parseInt(allMagazine.getQuantity()) + Integer.parseInt(quantity));
                isPresent = true;
            }
        }
        if (!isPresent) {
            if (number.matches(RegexExpressions.getRegexNumberMagazine()) && quantity.matches(RegexExpressions.getRegexQuantity())) {
                magazines.add(new Magazine(title, quantity, number));
            } else
                System.out.println("Incorrect value has been put. For number please use format 'mm/yyyy', for quantity only numbers from range '1-1000')");
        }
        saveMagazinesIntoFile();
    }

    public void saveMagazinesIntoFile() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(path + "magazines.txt"));
        String contentFile = "Title;NumberMagazine;Quantity\r\n";
        try {

            for (Magazine allMagazine : magazines) {
                contentFile += (allMagazine.getTitle() + ";" + allMagazine.getQuantity() + ";" + allMagazine.number + "\r\n");
            }
            bw.write(contentFile);
        } finally {
            bw.close();
        }
    }

    public void addUsers(String name, String surname, String cardID, String type) throws IOException {
        boolean isPresent = false;
        for (User allUser : users) {
            if ((allUser.getCardId().equals(cardID))) {
                System.out.println("User is already registered in database");
                isPresent = true;
            }
        }
        if (!isPresent) {
            if (name.matches(RegexExpressions.getRegexName()) && surname.matches(RegexExpressions.getRegexName()) && cardID.matches(RegexExpressions.getRegexCardId())) {
                users.add(new User(name, surname, cardID, type));
                System.out.println("User has been added correctly");
            } else
                System.out.println("Incorrect value has been put. For name and surname you can only use letters, for cardID only numbers from range '1000-9999')");
        }
        saveUsersIntoFile();
    }

    public void saveUsersIntoFile() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(path + "users.txt"));
        String contentFile = "Name;Surname;CardId;Type\r\n";
        try {

            for (User allUser : users) {
                contentFile += (allUser.getName() + ";" + allUser.getSurname() + ";" + allUser.getCardId() + ";" + allUser.getType() + "\r\n");
            }
            bw.write(contentFile);
        } finally {
            bw.close();
        }
    }

    String[] readFile(String file) throws IOException {
        String[] contentFile;
        BufferedReader br = new BufferedReader(new FileReader(path + file + ".txt"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (true) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                if (line == null) {
                    break;
                }
                switch (file) {
                    case "books":
                        books.add(new Book(line.split(";")[0], line.split(";")[1], line.split(";")[2]));
                        break;
                    case "magazines":
                        magazines.add(new Magazine(line.split(";")[0], line.split(";")[1], line.split(";")[2]));
                        break;
                    case "users":
                        users.add(new User(line.split(";")[0], line.split(";")[1], line.split(";")[2], line.split(";")[3]));
                        break;
                    case "borrowItems":
                        borrowItems.add(new BorrowItem(line.split(";")[0], line.split(";")[1], line.split(";")[2]));
                        break;
                }
            }
            contentFile = sb.toString().split("\r\n");
        } finally {
            br.close();
        }
        return contentFile;
    }

    public void displayBooks() {
        for (Book allBook : books) {
            System.out.println(allBook.getTitle() + " " + allBook.getAuthor() + " " + allBook.getQuantity());
        }
    }

    public void displayMagazines() {
        for (Magazine allMagazine : magazines) {
            System.out.println(allMagazine.getTitle() + " " + allMagazine.number + " " + allMagazine.getQuantity());
        }
    }

    public void displayUsers() {
        for (User allUser : users) {
            System.out.println(allUser.getName() + " " + allUser.getSurname() + " " + allUser.getCardId() + " " + allUser.getType());
        }
    }

    public void displayBorrowedOnes() {
        for (BorrowItem allBorrowItem : borrowItems) {
            System.out.println(allBorrowItem.getCardId() + " " + allBorrowItem.getTitle() + " " + allBorrowItem.getAuthorNumber());
        }
    }

//    public boolean isUserIsAbleToBorrow(String cardId) throws IOException {
//        int maxNumberForBorrow = MAX_NUMBER_FOR_BORROW_STUDENTS;
//        int borrowedOnes = 0;
//        for (User allUser : users) {
//            if (allUser.getType().equals("P")) {
//                maxNumberForBorrow = MAX_NUMBER_FOR_BORROW_PROFESORS;
//            }
//        }

//        BufferedReader br = new BufferedReader(new FileReader(path + "borrowItems.txt"));
//        try {
//            StringBuilder sb = new StringBuilder();
//            String line = br.readLine();
//            while (true) {
//                sb.append(line);
//                sb.append(System.lineSeparator());
//                line = br.readLine();
//                if (line == null) {
//                    break;
//                }
//                if (line.split(";")[0].equals(cardId)) {
//                    borrowedOnes++;
//                }
//            }
//        } finally {
//            br.close();
//        }
//        return borrowedOnes < maxNumberForBorrow;
//    }


//    public boolean isUserIsRegistered(String cardId) {
//        boolean isPresent = false;
//        for (User allUser : users) {
//            if ((allUser.getCardId().equals(cardId))) {
//                System.out.println("User exist. Name:  " + allUser.getName() + " surname: " + allUser.getSurname());
//                System.out.println("Is present " + isPresent);
//                isPresent = true;
//            } else {
//                System.out.println(isPresent + " surname " + allUser.getSurname());
//                System.out.println("Is present 2 " + isPresent);
//                isPresent = false;
//            }
//        }
//        System.out.println("Is present 3 " + isPresent);
//        return isPresent;
//
//    }

    public boolean isUserIsAbleToBorrow(String cardId) throws IOException {
        int maxNumberForBorrow = MAX_NUMBER_FOR_BORROW_STUDENTS;
        int borrowedOnes = 0;

        for (User allUser : users) {
            if ((allUser.getCardId().equals(cardId))) {
                if (allUser.getType().equals("P")) {
                    maxNumberForBorrow = MAX_NUMBER_FOR_BORROW_PROFESORS;
                }
            }
        }

        BufferedReader br = new BufferedReader(new FileReader(path + "borrowItems.txt"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (true) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                if (line == null) {
                    break;
                }
                if (line.split(";")[0].equals(cardId)) {
                    borrowedOnes++;
                }
            }
        } finally {
            br.close();
        }
        return borrowedOnes < maxNumberForBorrow;
    }

//    public void borrowItem(String cardId, String title, String authorNumber) throws IOException {
//
//        boolean isAvailaibleForBorrow = false;
//
//        for (Book allBook : books) {
//            System.out.println(allBook.getTitle());
//            if ((allBook.getTitle().contains(title)) && (allBook.getAuthor().contains(authorNumber))) {
//                System.out.println(allBook.getTitle());
//                System.out.println(title);
//                allBook.quantity = Integer.toString(Integer.parseInt(allBook.getQuantity()) - 1);
//                saveBooksIntoFile();
//            } else
//            isAvailaibleForBorrow = false;
//        }
//
//        for (Magazine allMagazine : magazines) {
//            if ((allMagazine.getQuantity().contains(title)) && (allMagazine.number.contains(authorNumber))) {
//                allMagazine.quantity = Integer.toString(Integer.parseInt(allMagazine.getQuantity()) - 1);
//                isAvailaibleForBorrow = true;
//                saveMagazinesIntoFile();
//            } else
//                isAvailaibleForBorrow = false;
//        }
//
//        if (!isAvailaibleForBorrow) {
//            System.out.println("There is no availaible books/magazines with title: " + title + " number " + authorNumber);
//        }
//
//        if (isAvailaibleForBorrow) {
//            borrowItems.add(new BorrowItem(cardId, title, authorNumber));
//            BufferedWriter bw = new BufferedWriter(new FileWriter(path + "borrowItems.txt"));
//            String contentFile = "cardId;Title;Author\\Number\r\n";
//            try {
//                for (BorrowItem allBorrowItem : borrowItems) {
//                    contentFile += (allBorrowItem.getCardId() + ";" + allBorrowItem.getTitle() + ";" + allBorrowItem.getAuthorNumber() + "\r\n");
//                }
//                bw.write(contentFile);
//            } finally
//
//            {
//                bw.close();
//            }
//        }
//    }
//}

    public void borrowItem(String cardId, String title, String authorNumber) throws IOException {

//        boolean isPresent = false;
//        for (User allUser : users) {
//            if ((allUser.getCardId().equals(cardId))) {
//                isPresent = true;
//            }
//        }


        for (Book allBook : books) {
            if ((allBook.getTitle().equals(title)) && (allBook.getAuthor().equals(authorNumber))) {
                allBook.quantity = Integer.toString(Integer.parseInt(allBook.quantity) - 1);
                System.out.println("User has borrow " + allBook.getTitle() + " " + allBook.getAuthor());
                saveBooksIntoFile();
                borrowItems.add(new BorrowItem(cardId, title, authorNumber));
                BufferedWriter bw = new BufferedWriter(new FileWriter(path + "borrowItems.txt"));
                String contentFile = "cardId;Title;Author\\Number\r\n";
                try {

                    for (BorrowItem allBorrowItem : borrowItems) {
                        contentFile += (allBorrowItem.getCardId() + ";" + allBorrowItem.getTitle() + ";" + allBorrowItem.getAuthorNumber() + "\r\n");
                    }
                    bw.write(contentFile);
                } finally {
                    bw.close();
                }
            }
        }
        for (Magazine allMagazine : magazines) {
            if ((allMagazine.getTitle().equals(title)) && (allMagazine.number.equals(authorNumber))) {
                allMagazine.quantity = Integer.toString(Integer.parseInt(allMagazine.quantity) - 1);
                System.out.println("User has borrow " + allMagazine.getTitle() + " " + allMagazine.getAuthor());
                saveMagazinesIntoFile();
                borrowItems.add(new BorrowItem(cardId, title, authorNumber));
                BufferedWriter bw = new BufferedWriter(new FileWriter(path + "borrowItems.txt"));
                String contentFile = "cardId;Title;Author\\Number\r\n";
                try {

                    for (BorrowItem allBorrowItem : borrowItems) {
                        contentFile += (allBorrowItem.getCardId() + ";" + allBorrowItem.getTitle() + ";" + allBorrowItem.getAuthorNumber() + "\r\n");
                    }
                    bw.write(contentFile);
                } finally {
                    bw.close();
                }
            }
        }
    }
}




