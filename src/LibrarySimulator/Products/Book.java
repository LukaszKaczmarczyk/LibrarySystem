package LibrarySimulator.Products;

public class Book {

    private String title;
    private String author;
    String quantity;

    public Book(String title, String author, String quantity) {
        this.title = title;
        this.author = author;
        this.quantity = quantity;
    }

    public Book(String title, String quantity) {
        this.title = title;
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getQuantity() {
        return quantity;
    }
}
