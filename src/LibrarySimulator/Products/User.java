package LibrarySimulator.Products;

public class User extends BorrowItem {

    private String name;
    private String surname;
    private String type;

    public User(String name, String surname, String cardId, String type) {
        super(cardId);
        this.name = name;
        this.surname = surname;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getType() {
        return type;
    }
}
