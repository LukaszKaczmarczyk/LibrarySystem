package LibrarySimulator;

import LibrarySimulator.Products.Library;

import java.io.IOException;

public class MainSystem {

    public static void main(String[] args) throws IOException {
        Library libraryMenu = new Library();
        libraryMenu.runInterfaceMenu();
    }
}
