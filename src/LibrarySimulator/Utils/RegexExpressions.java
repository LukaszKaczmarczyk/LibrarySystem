package LibrarySimulator.Utils;

public class RegexExpressions {

    private static String regexName = "^[a-zA-Z ]+$";
    private static String regexQuantity = "^([1-9][0-9]{0,2}|1000)$";
    private static String regexNumberMagazine = "^(1[0-2]|0[1-9])/(19[0-9][0-9]|20[0-1][0-8])$";
    private static String regexCardId = "^([1-9][0-9][0-9][0-9])$";

    public static String getRegexName() {
        return regexName;
    }

    public static String getRegexQuantity() {
        return regexQuantity;
    }

    public static String getRegexNumberMagazine() {
        return regexNumberMagazine;
    }

    public static String getRegexCardId() {
        return regexCardId;
    }
}
