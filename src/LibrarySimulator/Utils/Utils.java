package LibrarySimulator.Utils;

public class Utils {

    public static void displayOptionMenu(){
        System.out.println("Welcome at Library. Please follow the options");
        System.out.println("Enter 1 for display all books");
        System.out.println("Enter 2 for display magazines");
        System.out.println("Enter 3 for add book to library");
        System.out.println("Enter 4 for add magazine to library");
        System.out.println("Enter 5 to display users");
        System.out.println("Enter 6 add user");
        System.out.println("Enter 7 open file with borrowed items");
        System.out.println("Enter 8 for borrow the book/magazine");
        System.out.println("Enter 9 to quit the program");
        System.out.println();
    }
}
